# Install

SRCLOC=`dirname $0`

sh $SRCLOC/install-gentools.sh
sh $SRCLOC/install-zsh.sh
sh $SRCLOC/install-vim.sh
